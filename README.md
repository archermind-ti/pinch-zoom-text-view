# PinchZoomTextView

#### 简介
这个库允许你拥有一个文本，它将使用用户的手势来增加/缩小字体大小.

#### 功能
1. 手指手势来增加/减小字体大小的文本库

#### 演示
![输入图片说明](https://gitee.com/archermind-ti/pinch-zoom-text-view/raw/master/sample.gif "在这里输入图片标题")

#### 集成

1. 下载模块包关联使用
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar'])
    implementation project(':lib')
    testImplementation 'junit:junit:4.13'
}
```
2.maven管理
```
...
allprojects {
    repositories {
     mavenCentral()
    }
}
...
dependencies {
 ...
 implementation 'com.gitee.archermind-ti:pinchzoomtextview:1.0.1'
 ...
}
```

#### 使用说明

```
 final PinchZoomTextView textView = (PinchZoomTextView) findComponentById(ResourceTable.Id_text_helloworld);
        Font font=new Font.Builder(textView.getFont().getName()).setWeight(100).build();
        textView.setFont(font);
        final Button enableToggle = (Button) findComponentById(ResourceTable.Id_zoom_toggle);
        enableToggle.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                boolean newEnabledState = !textView.isZoomEnabled();
                textView.setZoomEnabled(newEnabledState);
                enableToggle.setText(
                        newEnabledState ? "zoom_enabled": "zoom_disabled"
                );
            }
        });
```
#### 编译说明
1. 将项目通过git clone 至本地
2. 使用DevEco Studio 打开该项目，然后等待Gradle 构建完成
3. 点击Run运行即可（真机运行可能需要配置签名）

#### 版本迭代
- v1.0   初始版本
- v1.0.1   更改了包名
[changelog](https://gitee.com/archermind-ti/pinch-zoom-text-view/blob/master/changelog.md)

#### 版权和许可信息
#### License
PinchZoomTextView is available under the MIT License. You are free to do with it what you want. If you submit a pull request, please add your name to the credits section. :)