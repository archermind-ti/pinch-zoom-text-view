package com.ohosessence.pinchzoomtextview.sample;

import com.ohosessence.pinchzoomtextview.sample.ResourceTable;
import com.ohosessence.pinchzoomtextview.PinchZoomTextView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.text.Font;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
//        super.setMainRoute(MainAbilitySlice.class.getName());
        super.setUIContent(ResourceTable.Layout_ability_main);
        final PinchZoomTextView textView = (PinchZoomTextView) findComponentById(ResourceTable.Id_text_helloworld);
        Font font=new Font.Builder(textView.getFont().getName()).setWeight(100).build();
        textView.setFont(font);
        final Button enableToggle = (Button) findComponentById(ResourceTable.Id_zoom_toggle);
        enableToggle.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                boolean newEnabledState = !textView.isZoomEnabled();
                textView.setZoomEnabled(newEnabledState);
                enableToggle.setText(
                        newEnabledState ? "zoom_enabled": "zoom_disabled"
                );
            }
        });
    }
}
